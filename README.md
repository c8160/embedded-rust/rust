# rust

rust based on Alpine Linux.

> This image is tested for use with **podman** on **Linux** hosts with **x86_64** arch


## What is rust?

Rust is a systems programming language sponsored by Mozilla Research. It is
designed to be a "safe, concurrent, practical language", supporting functional
and imperative-procedural paradigms. Rust is syntactically similar to C++, but
is designed for better memory safety while maintaining performance.

See [the project website](https://github.com/rust-lang/rust).


## How to use this image

> Keep in mind that this is an image meant specifically for embedded
> development. To build applications for your host OS natively, prefer the
> official rust containers from the Docker Hub.


### Replacement for natively installing rust

```bash
$ podman run --rm -it -v "cargo-registry:/usr/share/cargo/registry" -v "$PWD:/project:z" registry.gitlab.com/c8160/embedded-rust/rust [cargo|rust] ...
```

This will run either cargo or rust (at your option) with any arguments you
supply. If you want to build a project you must call the command from the
projects root directory (where your `Cargo.toml` is located). Since only your
`$PWD` is mounted into the container, it will not be able to find your
`Cargo.toml` in directories further up in the file tree.


### Running as full user installation

```bash
$ podman run --rm -it --user "$(id -u):$(id -g)" --userns keep-id -v "cargo-registry:/usr/share/cargo/registry" -v "$PWD:/project:z" registry.gitlab.com/c8160/embedded-rust/rust [cargo|rust] ...
```

We add the `--user` and `--userns` flags to make the container act as the user
that is executing it. If this is omitted, files created inside the container
may belong to root and are thus inaccessible to the user afterwards.


### Running with correct file-paths

```bash
$ podman run --rm -it --user "$(id -u):$(id -g)" --userns keep-id -v "cargo-registry:/usr/share/cargo/registry" -v "$PWD:$PWD:z" -w "$PWD" registry.gitlab.com/c8160/embedded-rust/rust [cargo|rust] ...
```

We modify the second `-v` and add the `-w` argument to tell the container to
mount out current `$PWD` at the path `$PWD` in the container and use this as
working directory (instead of the default `/project`).

This is a purely cosmetic change and doesn't affect the programs execution in
any way. It merely makes identifying the host paths easier.



## Getting help

If you feel that something isn't working as expected, open an issue in the
Gitlab project for this container and describe what issue you are facing.

