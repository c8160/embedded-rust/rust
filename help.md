rust image, for developing on embedded systems.

Packages the rust language toolchain with additional embedded targets for
cross-compilation.

Volumes:
cargo-registry: Mapped to /usr/local/cargo/registry, contains the cargo
  registry cache. This is completely optional, but massively speeds up
  successive calls to the container. If it is ommited, cargo rebuilds its cache
  on every invocation of cargo.
PWD: The current directory, which should include a cargo project. Note that the
  `Cargo.toml` to build the project with must be accessible from within this
  directory, since cargo won't be able to climb further up the file tree.

Documentation:
For the underlying rust project, see https://github.com/rust-lang/rust
For this container, see https://gitlab.com/c8160/embedded-rust/rust

Requirements:
Needs a rust project to work with.

Configuration:
None

