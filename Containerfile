FROM docker.io/library/rust:1.60-alpine

ENV NAME=rust VERSION=0 ARCH=x86_64
LABEL   name="$NAME" \
        version="$VERSION" \
        architecture="$ARCH" \
        run="podman run --rm -it -v 'cargo-registry:/usr/local/cargo/registry' -v \"$PWD:/project:z\" IMAGE [cargo|rust] ..." \
        summary="Rust toolchain for embedded development (Cortex-M4F/M7F)" \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/embedded-rust/rust"

# Add documentation
COPY help.md /

RUN rustup target add thumbv7em-none-eabihf; \
    rustup component add llvm-tools-preview

RUN apk add musl-dev openssl-dev && rm -rf /var/cache/apk/*; \
    rm -rf /usr/local/cargo/registry

WORKDIR /project
VOLUME /project
VOLUME /usr/local/cargo/registry

# We specify no entrypoint here, because we leave this to the user depending on
# the tool they need.
